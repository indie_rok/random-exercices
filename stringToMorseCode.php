
<!-- This program translates any string of text. No numbers .and returns it to morse code  -->

<h1>
<?php stringToMorse("hola mundo ks");?> </h1>
<?php
function stringToMorse($string){

$morseCode = array("a" => ".-","b" => "-...","c" => "-.-.","d" => "-..","e" => ".","f" => "..-.",
"g" => "--.","h" => "....","i" => "..","j" => ".---","k" => "-.-","l" => ".-..","m" => "--","n" => "-.",
"o" => "---","p" => ".--.","q" => "--.-","r" => ".-.","s" => "....","t" => "-","u" => "..-","v" => "...-",
"w" => ".--","x" => "-..-","y" => "-.--","z" => "--..");

	$stringToArray = str_split($string);
	$morseResult = array();
	
	
	for($i=0;$i<count($stringToArray);$i++){	
		if($stringToArray[$i] == " "){
			array_push($morseResult,"  "." ");
		}
		else{
			array_push($morseResult,$morseCode[$stringToArray[$i]]." ");	
		}
	}
	
	return implode("",$morseResult);
}