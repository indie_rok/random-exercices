<?php

/* this algorithm takes a string of multiple words
and returns only the words sorted alphabeticly

For ex:
Hello i am Abno

Should return only
am Abno

*/

echo orderVowels("Hello i am Abno");

function orderVowels($string){
	
	$string = strtolower($string);
	
	$arrayString = explode(" ",$string);
	$finalArray = array();
	
	for($i=0;$i<count($arrayString);$i++){
		if(wordIsOrdered($arrayString[$i])){
			array_push($finalArray,$arrayString[$i]);
		}
	}
	
	return implode(" ",$finalArray);
}

function wordIsOrdered($word){
	$array_words = str_split($word);
	$abecedary = array("a" => 0,"b"=> 1,"c"=> 2,"d"=> 3,
	"e"=> 4,"f"=> 5,"g"=> 6,"h"=>7,"i"=> 8,"j"=> 9,"k"=> 10,
	"l"=> 11,"m"=> 12,"n"=> 13,"o"=> 14,"p"=> 15,"q"=> 16,"r"=> 17,
	"s"=> 18,"t"=> 19,"u"=> 20,"v"=> 21,"w"=> 22,"x"=> 23,"y"=> 24,"z"=> 25);
	
	for($i=0;$i<strlen($word)-1;$i++){
		if($abecedary[$array_words[$i]] > $abecedary[$array_words[$i+1]])
		{
			return false;
		}
		else{
			if($i == strlen($word)-2){
				return true;
			}
		}
	}
	
}
