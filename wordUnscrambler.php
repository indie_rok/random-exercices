<?php

/*This algoritm takes one word and a diccionary of word (array)
it returns the anagrams of each word*/

wordUnscrambler("turn", ["numb", "turn", "runt", "nurt"]);


function wordUnscrambler($word,$dictionary){
	
	$arrayFinal = array();
	
	for($i=0;$i<count($dictionary);$i++){
		
		if(isAnagram($word,$dictionary[$i])){
			array_push($arrayFinal,$dictionary[$i]);
		}
	}
	
	print_r($arrayFinal);
}

function isAnagram($word,$wordDiccionary){
	$arrayWord= str_split($word);
	$arrayDiccionary = str_split($wordDiccionary);
	
	sort($arrayWord);
	sort($arrayDiccionary);
	
	if($arrayDiccionary == $arrayWord){
		return true;
	}
	else{
		return false;	
	}
}